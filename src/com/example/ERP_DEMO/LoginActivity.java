package com.example.ERP_DEMO;

import android.app.Activity;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

/**
 * Created with IntelliJ IDEA.
 * User: arthur
 * Date: 2/11/13
 * Time: 8:53 PM
 * To change this template use File | Settings | File Templates.
 */
public class LoginActivity extends Activity {

    private boolean loginSuccess = true;

    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login);

        final EditText editTextUsername = (EditText) findViewById(R.id.editTextUsername);
        final EditText editTextPassword = (EditText) findViewById(R.id.editTextPassword);

        Button buttonSubmit = (Button) findViewById(R.id.buttonSubmit);
        Button buttonClear = (Button) findViewById(R.id.buttonClear);

        buttonSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(!editTextPassword.getText().toString().isEmpty() && !editTextUsername.getText().toString().isEmpty()){
                    LoginService loginService = new LoginService(editTextUsername.getText().toString(), editTextPassword.getText().toString());
                    loginService.setActivity(LoginActivity.this);
                    loginService.execute();

                 }
                else{
                    //Display Toaster for error
                    Toast toast = Toast.makeText(getApplicationContext(), "Please enter your details", Toast.LENGTH_SHORT);
                    toast.setGravity(Gravity.CENTER_HORIZONTAL, 0, 5);
                    toast.show();
                }
            }
        });


        buttonClear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(getApplicationContext(),"Cleared...", Toast.LENGTH_LONG).show();
               editTextUsername.setText("");
               editTextPassword.setText("");
            }
        });
    }

    public Boolean setLoginSuccessful(boolean newValue){
        return loginSuccess = !loginSuccess;
    }
}