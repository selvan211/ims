package com.example.ERP_DEMO;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;
import com.google.gson.Gson;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.CookieStore;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.protocol.ClientContext;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: arthur
 * Date: 2/26/13
 * Time: 10:49 PM
 * To change this template use File | Settings | File Templates.
 */
public class InterventionService extends AsyncTask<Intervention, Integer,Void> {

    private static final String submitIntervention_URL = "http://mobile.dcl.mu/webserver/submitIntervention.php";
    private Intervention intervention;
    private int itvr_id;
    private String case_id;
    private int success;
    private Activity context;
    private ProgressDialog progressDialog;
    private String mysql_message;

    public InterventionService(Intervention intervention){
        this.intervention=intervention;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();    //To change body of overridden methods use File | Settings | File Templates.
        progressDialog = ProgressDialog.show(context,"Saving intervention" ,"Please wait...");
    }


    @Override
    protected void onProgressUpdate(Integer... values) {
        super.onProgressUpdate(values);    //To change body of overridden methods use File | Settings | File Templates.


    }

    @Override
    protected Void doInBackground(Intervention... interventions) {

        HttpClient httpClient = new DefaultHttpClient();
        HttpPost httpPost = new HttpPost(submitIntervention_URL);
        HttpParams httpParams = new BasicHttpParams();
        HttpConnectionParams.setConnectionTimeout(httpParams, 10000);
        HttpConnectionParams.setSoTimeout(httpParams,10000);

        Gson gson = new Gson();
        //intervention.setCust_sign(intervention.getCust_sign().replace("\n",""));
        //intervention.setUser_sign(intervention.getUser_sign().replace("\n", ""));
        intervention.setDate_created(intervention.getDate_created().replace("/","."));
        String json = gson.toJson(intervention);

        Log.i("Result from web-service",json);

        try {

            List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
            nameValuePairs.add(new BasicNameValuePair("intervention",json));
            //nameValuePairs.add(new BasicNameValuePair("customer_signature",intervention.getCust_sign()));
            //nameValuePairs.add(new BasicNameValuePair("user_signature",intervention.getUser_sign()));
            nameValuePairs.add((new BasicNameValuePair("date_created", intervention.getDate_created())));
            httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs));

            CookieStore cookieStore = new BasicCookieStore();
            HttpContext httpContext = new BasicHttpContext();
            httpContext.setAttribute(ClientContext.COOKIE_STORE,cookieStore);

            HttpResponse httpResponse = httpClient.execute(httpPost,httpContext);

            HttpEntity entity = httpResponse.getEntity();

            if (entity != null) {
              String result = EntityUtils.toString(httpResponse.getEntity());
                JSONObject jsonObject = new JSONObject(result);
                //JSONArray jsonArray = jsonObject.getJSONArray("Response");
                //JSONObject responseID = jsonArray.getJSONObject(0);
                //jsonObject.getInt("success")

                success = jsonObject.getInt("success");
                //This means that the insertion has been successful
                if(success==1){
                    itvr_id = jsonObject.getInt("id_of_itvr");
                    case_id = intervention.getCase_id();

                }else{
                    mysql_message = jsonObject.getString("mysql_message");
                    Log.e("ITVR insertion", jsonObject.getString("mysql_message"));
                }

            }
            }catch (IOException e){
            Log.e("Cases_Service", e.toString());
        } catch (JSONException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }

        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);
        //LoginSuccess == 1 when the login is successful
        if (success==1){
            Intent interventionDisplay = new Intent(context, InterventionDisplayActivity.class);
            interventionDisplay.putExtra("itvr_id", itvr_id);
            interventionDisplay.putExtra("case_id", case_id);
            interventionDisplay.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            interventionDisplay.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(interventionDisplay);
            context.finish();
        }else{
            Toast.makeText(context, "Error inserting into database: mysql_message", Toast.LENGTH_LONG).show();
        }
        progressDialog.dismiss();
    }


    public void setActivity(Activity newContext){
        context = newContext;
    }


}
