package com.example.ERP_DEMO;

import java.io.Serializable;

/**
 * Created with IntelliJ IDEA.
 * User: arthur
 * Date: 2/19/13
 * Time: 10:41 PM
 * To change this template use File | Settings | File Templates.
 */
public class Case implements Serializable {
    private String case_id;
    private String account_id;
    private String references;
    private String case_name;
    private String case_details;
    private String date_created;
    private String date_modified;
    private String team_name;
    private String account_name;
    private String activity_type;
    private String priority;
    private String c_user;
    private String m_user;

    public Case(){}

    public String getCase_details() {
        return case_details;
    }

    public void setCase_details(String case_details) {
        this.case_details = case_details;
    }

    public String getCase_id() {
        return case_id;
    }

    public void setCase_id(String case_id) {
        this.case_id = case_id;
    }

    public String getAccount_id() {
        return account_id;
    }

    public void setAccount_id(String account_id) {
        this.account_id = account_id;
    }

    public String getReferences() {
        return references;
    }

    public void setReferences(String references) {
        this.references = references;
    }

    public String getCase_name() {
        return case_name;
    }

    public void setCase_name(String case_name) {
        this.case_name = case_name;
    }

    public String getDate_created() {
        return date_created;
    }

    public void setDate_created(String date_created) {
        this.date_created = date_created;
    }

    public String getDate_modified() {
        return date_modified;
    }

    public void setDate_modified(String date_modified) {
        this.date_modified = date_modified;
    }

    public String getTeam_name() {
        return team_name;
    }

    public void setTeam_name(String team_name) {
        this.team_name = team_name;
    }

    public String getAccount_name() {
        return account_name;
    }

    public void setAccount_name(String account_name) {
        this.account_name = account_name;
    }

    public String getActivity_type() {
        return activity_type;
    }

    public void setActivity_type(String activity_type) {
        this.activity_type = activity_type;
    }

    public String getPriority() {
        return priority;
    }

    public void setPriority(String priority) {
        this.priority = priority;
    }

    public String getC_user() {
        return c_user;
    }

    public void setC_user(String c_user) {
        this.c_user = c_user;
    }

    public String getM_user() {
        return m_user;
    }

    public void setM_user(String m_user) {
        this.m_user = m_user;
    }

    @Override
    public String toString() {
        return  references + "\n" + "\t" +case_name + "\n" +"\t \t"+ account_name;
    }
}
