package com.example.ERP_DEMO;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

public class CasesActivity extends Activity {

    //static TextView textView;
    private static TextView casesAmount;
    private ListView listView;
    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.show_cases);

        Intent i = getIntent();
        String username = i.getStringExtra("username");
        final String userID = i.getStringExtra("userId");
        String session_id = i.getStringExtra("session_id");

        //Running the CasesService
        CasesService casesService = new CasesService(userID, session_id);
        casesService.setActivityContext(CasesActivity.this);
        casesService.execute();

        //Setting the textView for the username
        casesAmount = (TextView) findViewById(R.id.casesAmount);
        TextView textView = (TextView) findViewById(R.id.textView);
        textView.setText(userID+" : "+username);
        listView = (ListView) findViewById(R.id.listView);


        //Button buttonConnect = (Button) findViewById(R.id.buttonConnect);

        //Creating the Adapter for the ListView
        final ArrayList<Case> casesList = casesService.getCasesList();
        listView.setClickable(true);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                //To change body of implemented methods use File | Settings | File Templates.
                    //Toast.makeText(CasesActivity.this, "Loading Cases", Toast.LENGTH_LONG).show();
                        Case aCase = casesList.get(i);
                        Intent caseInformation = new Intent(getApplicationContext(), CaseInformationActivity.class);
                        caseInformation.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        caseInformation.putExtra("Case", aCase);
                        caseInformation.putExtra("userID",userID);
                        startActivity(caseInformation);
                    }

        });
    }

    public void setCasesAccount(String newString){
        casesAmount.setText(newString);
    }

    public void setListViewAdapter(ArrayAdapter<Case> adapter){
        listView.setAdapter(adapter);
    }

}
