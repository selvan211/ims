package com.example.ERP_DEMO;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.CookieStore;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.protocol.ClientContext;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: arthur
 * Date: 3/12/13
 * Time: 3:52 PM
 * To change this template use File | Settings | File Templates.
 */
public class InterventionReportListService extends AsyncTask<Void, Void, Void> {

    private String intervention_list_URL ="http://mobile.dcl.mu/webserver/intervention_list.php";
    private String case_id;
    public ArrayList<Intervention> interventionArrayList = new ArrayList<Intervention>();
    private ProgressDialog progressDialog;
    private Activity activity;
    private Dialog dialog;

    public InterventionReportListService(String case_id){
        this.case_id = case_id;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();    //To change body of overridden methods use File | Settings | File Templates.
        progressDialog = ProgressDialog.show(activity, "", "Loading the list of intervention reports");
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);    //To change body of overridden methods use File | Settings | File Templates.

        ListView listView = (ListView) dialog.findViewById(R.id.itvr_listView);

        //Setting the Adapter and the list with the correct information
        Collections.reverse(interventionArrayList);
        ArrayAdapter<Intervention> adapter = new ArrayAdapter<Intervention>(activity, android.R.layout.simple_list_item_1, interventionArrayList);
        listView.setAdapter(adapter);

        progressDialog.dismiss();

        if(interventionArrayList.isEmpty()){
            Toast.makeText(activity.getApplicationContext(), "No Interventions Reports for this Case", Toast.LENGTH_LONG).show();
            dialog.dismiss();
        }

    }

    @Override
    protected void onProgressUpdate(Void... values) {
        super.onProgressUpdate(values);    //To change body of overridden methods use File | Settings | File Templates.
    }

    @Override
    protected Void doInBackground(Void... voids) {

        HttpClient httpClient = new DefaultHttpClient();
        HttpPost httpPost = new HttpPost(intervention_list_URL);
        HttpParams httpParams = new BasicHttpParams();
        HttpConnectionParams.setConnectionTimeout(httpParams, 10000);
        HttpConnectionParams.setSoTimeout(httpParams,10000);

        try {

            List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
            nameValuePairs.add(new BasicNameValuePair("case_id", case_id));
            httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs));

            CookieStore cookieStore = new BasicCookieStore();
            HttpContext httpContext = new BasicHttpContext();
            httpContext.setAttribute(ClientContext.COOKIE_STORE,cookieStore);

            HttpResponse httpResponse = httpClient.execute(httpPost,httpContext);

            HttpEntity entity = httpResponse.getEntity();

            if (entity != null) {
                String result = EntityUtils.toString(httpResponse.getEntity());
                JSONObject jsonObject = new JSONObject(result);

                JSONArray interventionArray = jsonObject.getJSONArray("Interventions");

                interventionArrayList.clear();

                for(int i = 0;i<interventionArray.length();i++){

                    Intervention intervention = new Intervention();

                    JSONObject  object = interventionArray.getJSONObject(i);

                    String test = "";

                        if(object.getString("success").equals("0")){
                            break;
                        }else{
                            intervention.setCase_id(case_id);
                            intervention.setItvr_id(object.getString("itvr_id"));
                            intervention.setDescription(object.getString("description"));

                            String date_format = object.getString("date_created");
                            String[] date_created = date_format.split(" ");
                            intervention.setDate_created(date_created[0]);

                            interventionArrayList.add(intervention);
                        }

                        /*SimpleDateFormat myFormat = new SimpleDateFormat("dd-MM-yyyy");
                        String test;

                         try {

                            test = myFormat.format(myFormat.parse(date_format));
                          } catch (ParseException e) {
                            e.printStackTrace();
                          }*/
                    }

            }else{
                Log.e("HttpResponse incorrect","Entity null");
            }

        }catch (IOException e){
            Log.e("Intervention Report List ", e.toString());
        } catch (JSONException e) {
            Log.e("Intervention Report List ", e.toString());
        }
        return null;
    }

    public void setActivity(Activity activity) {
        this.activity = activity;
    }

    public void setDialog(Dialog dialog) {
        this.dialog = dialog;
    }

    public ArrayList<Intervention> getInterventionArrayList() {
        return interventionArrayList;
    }
}