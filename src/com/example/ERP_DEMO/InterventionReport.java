package com.example.ERP_DEMO;

import android.app.Activity;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.util.Base64;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.*;

import java.io.ByteArrayOutputStream;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created with IntelliJ IDEA.
 * User: arthur
 * Date: 2/22/13
 * Time: 12:25 AM
 * To change this template use File | Settings | File Templates.
 */
public class InterventionReport extends Activity {

    private TextView itvrDate;
    private TextView arrivaltime;
    private TextView departuretime;
    private TimePickerDialog pickerDialog_arrival;
    private TimePickerDialog pickerDialog_departure;
    private boolean isUserSigning;
    private ImageView userSigImage;
    private ImageView custSigImage;
    private EditText description;
    private EditText solution;
    private EditText userRemarks;
    private EditText customerRemarks;
    private EditText partsDelivered;
    private Intervention intervention;
    private Case aCase;
    private String userID;
    private Spinner spinnerStatus;
    private CheckBox caseCompleted;
    private CheckBox chargeTransport;
    private CheckBox serviceBillable;
    private Button setArrivalTime;
    private Button setDepartureTime;
    private SimpleDateFormat dateFormat;
    private Button userSig;
    private Button customerSig;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.insert_intervention);

        //Retrieving the Case information from the previous activity
        Intent i = getIntent();
        aCase = (Case) i.getSerializableExtra("Case");
        intervention = new Intervention();
        userID = i.getStringExtra("userID");

        //The Date inserted
        itvrDate = (TextView) findViewById(R.id.date);
        dateFormat = new SimpleDateFormat("yyyy/MM/dd");
        itvrDate.setText(dateFormat.format(Calendar.getInstance().getTime()));

        TextView caseID = (TextView) findViewById(R.id.caseID);
        TextView accountName = (TextView) findViewById(R.id.accountName);

        description = (EditText) findViewById(R.id.description);
        solution = (EditText) findViewById(R.id.editSolution);
        userRemarks = (EditText) findViewById(R.id.userRemark);
        customerRemarks = (EditText) findViewById(R.id.customerRemark);
        partsDelivered = (EditText) findViewById(R.id.partdelivered);

        caseCompleted = (CheckBox) findViewById(R.id.checkBoxCaseComplete);
        chargeTransport = (CheckBox) findViewById(R.id.checkBoxChargeTransport);
        serviceBillable = (CheckBox) findViewById(R.id.checkBoxServiceBillable);

        setArrivalTime = (Button) findViewById(R.id.buttonArrival);
        setDepartureTime = (Button) findViewById(R.id.buttondeparture);
        userSig = (Button) findViewById(R.id.button_userSig);
        customerSig = (Button) findViewById(R.id.button_customerSig);

        userSigImage =(ImageView) findViewById(R.id.imageViewUserSign);
        custSigImage = (ImageView) findViewById(R.id.imageViewCustSign);

        spinnerStatus = (Spinner) findViewById(R.id.spinner_status);

        //Populating the View with the right information
        caseID.setText(aCase.getReferences());
        accountName.setText(aCase.getAccount_name());

        final ArrayAdapter<CharSequence> adapter_status = ArrayAdapter.createFromResource(this, R.array.itvr_status, android.R.layout.simple_spinner_dropdown_item);
        spinnerStatus.setAdapter(adapter_status);

        spinnerStatus.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                //Toast.makeText(InterventionReport.this,"I have clicked this: "+ adapter_status.getItem(i).toString() +" ", Toast.LENGTH_SHORT).show();
                if(adapter_status.getItem(i).toString().isEmpty()){
                    intervention.setStatus("");
                }else{
                    intervention.setStatus(adapter_status.getItem(i).toString());
                }

                if(adapter_status.getItem(i).toString().equals("Solved")){
                    caseCompleted.setChecked(true);
                    intervention.setCase_complete(true);
                }else{
                    intervention.setCase_complete(false);
                    caseCompleted.setChecked(false);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                //To change body of implemented methods use File | Settings | File Templates.
                Toast.makeText(InterventionReport.this,"Please Select a status", Toast.LENGTH_SHORT).show();
            }
        });

        //Creating the textView for the time
        arrivaltime = (TextView) findViewById(R.id.arrivalTime);
        departuretime = (TextView) findViewById(R.id.departuretime);


        final Calendar cal = Calendar.getInstance();

        setArrivalTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pickerDialog_arrival = new TimePickerDialog(view.getContext(), new TimeSetHandler(true, getApplicationContext())  ,cal.get(Calendar.HOUR_OF_DAY), cal.get(Calendar.MINUTE), true);
                pickerDialog_arrival.show();
            }
        });


        setDepartureTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //To change body of implemented methods use File | Settings | File Templates.
                pickerDialog_departure = new TimePickerDialog(view.getContext(), new TimeSetHandler(false, getApplicationContext()), cal.get(Calendar.HOUR_OF_DAY), cal.get(Calendar.MINUTE),true);
                pickerDialog_departure.show();
            }
        });


        /**
         * Checkboxes Scenarios - when clicked and checked - insert information in object intervention
         */
        caseCompleted.setEnabled(false);
        caseCompleted.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //To change body of implemented methods use File | Settings | File Templates.
                //Toast.makeText(view.getContext(),"You have clicked on it", Toast.LENGTH_SHORT).show();
                if(caseCompleted.isChecked()){
                    intervention.setCase_complete(true);
                }else{
                    intervention.setCase_complete(false);
                }
            }
        });

        chargeTransport.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               if(chargeTransport.isChecked()){
                    intervention.setCharge_transport(true);
                }else{
                    intervention.setCharge_transport(false);
                }
            }
        });

        serviceBillable.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //To change body of implemented methods use File | Settings | File Templates.
                if(serviceBillable.isChecked()){
                    intervention.setService_billable(true);
                }else{
                    intervention.setService_billable(false);
                }
            }
        });

        /**
         * The signatures of the user and customer
         */
        userSig.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                isUserSigning = true;
                Intent i = new Intent(InterventionReport.this, CaptureSignature.class);
                startActivityForResult(i, 0);
            }
        });

        customerSig.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String error_message=validateInterventionField(intervention);
                if(error_message.isEmpty()){
                    isUserSigning=false;
                    Intent i = new Intent(InterventionReport.this, CaptureSignature.class);
                    startActivityForResult(i, 0);
                }else{
                    Toast.makeText(InterventionReport.this, "Please fill the following: " +error_message +"", Toast.LENGTH_LONG).show();
                }



            }
        });

    }

    /**
     * Populating the object intervention with the right information
     */
    private void populateIntervention(){
        intervention.setCase_id(aCase.getCase_id());
        intervention.setAccount_id(aCase.getAccount_id());
        intervention.setUser_id(userID);
        intervention.setDescription(description.getText().toString());
        intervention.setSolution(solution.getText().toString());
        intervention.setUser_remarks(userRemarks.getText().toString());
        intervention.setCustomer_remarks(customerRemarks.getText().toString());
        intervention.setDate_created(dateFormat.format(Calendar.getInstance().getTime()));
        intervention.setParts_delivered(partsDelivered.getText().toString());
    }

    /**
     * Set all the fields except for Customer Signature non editable
     */
    private void setFieldsNonEditable(){
        description.setEnabled(false);
        solution.setEnabled(false);
        partsDelivered.setEnabled(false);
        userRemarks.setEnabled(false);
        spinnerStatus.setEnabled(false);
        chargeTransport.setEnabled(false);
        serviceBillable.setEnabled(false);
        caseCompleted.setEnabled(false);
        setArrivalTime.setEnabled(false);
        setDepartureTime.setEnabled(false);
        userSig.setEnabled(false);
        customerRemarks.setEnabled(false);

    }

    /**
     * Set all the fields editable
     */
    private void setFieldsEditable(){
        description.setEnabled(true);
        solution.setEnabled(true);
        partsDelivered.setEnabled(true);
        userRemarks.setEnabled(true);
        spinnerStatus.setEnabled(true);
        chargeTransport.setEnabled(true);
        serviceBillable.setEnabled(true);
        caseCompleted.setEnabled(true);
        setArrivalTime.setEnabled(true);
        setDepartureTime.setEnabled(true);
        userSig.setEnabled(true);
        customerRemarks.setEnabled(true);
    }

    /**
     * Validate if every field in intervention is not empty
     * @return String with the error_message
     */
    private String validateInterventionField(Intervention interventionReport){

        populateIntervention();

        String error_message ="";

        if(interventionReport.getDescription().isEmpty()){
            error_message = error_message + "Diagnostics \t";
        }
        if(interventionReport.getSolution().isEmpty()){
            error_message = error_message + ":Solution \t";
        }
        if(interventionReport.getStatus().isEmpty()){
            error_message = error_message + ":Status \t";
        }
        if(interventionReport.getArrival_time() == null){
            error_message = error_message + ":Arrival time \t";
        }
        if(interventionReport.getDeparture_time() == null){
            error_message = error_message + ":Departure time \t";
        }
        if(interventionReport.getUser_remarks().isEmpty()){
            error_message = error_message + ":Technician remarks \t";
        }
        if(interventionReport.getCustomer_remarks().isEmpty()){
            error_message = error_message + ":Customer remarks \t";
        }
        if(interventionReport.getUser_sign() == null){
            error_message = error_message + ":Technician signature \t";
        }
        if(interventionReport.getParts_delivered().isEmpty()){
            error_message += ": Parts delivered \t";
        }
        return error_message;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflator = getMenuInflater();
        inflator.inflate(R.menu.intervention_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem){
        //Handle item selection
        String error_message = validateInterventionField(intervention);
        switch (menuItem.getItemId()){
            case R.id.saveIntervention:

                if(!error_message.isEmpty()){
                    Toast.makeText(InterventionReport.this, "Please fill the following " +error_message +"", Toast.LENGTH_LONG).show();
                    setFieldsEditable();
                }else{
                    InterventionService interventionService = new InterventionService(intervention);
                    interventionService.setActivity(InterventionReport.this);
                    interventionService.execute();
                    //Toast.makeText(InterventionReport.this, "Saving intervention report...", Toast.LENGTH_LONG).show();
                }
                return true;
            default:
                return true;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (resultCode == 1) {
            Bitmap b = BitmapFactory.decodeByteArray(
                    data.getByteArrayExtra("byteArray"), 0,
                    data.getByteArrayExtra("byteArray").length);

            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            b.compress(Bitmap.CompressFormat.PNG,50,baos);
            byte[] bytes=baos.toByteArray();
            String base64Image = Base64.encodeToString(bytes,Base64.NO_WRAP);
            if(isUserSigning){
                userSigImage.setImageBitmap(b);
                intervention.setUser_sign(base64Image);
            }else{
                custSigImage.setImageBitmap(b);
                //According to DCL, once the customer has signed, the technician or customer is not allowed to change anything.
                if(!base64Image.isEmpty()){
                    setFieldsNonEditable();
                    intervention.setCust_sign(base64Image);
                }else {
                    Toast.makeText(InterventionReport.this, "Requires the customer's signature...", Toast.LENGTH_LONG).show();
                }
            }
        }
    }


    //Creating onTimeSet for TimePicker Dialogue
    private class TimeSetHandler implements TimePickerDialog.OnTimeSetListener{

        boolean isArrival;
        Context context;

        public TimeSetHandler(Boolean isArrival, Context context){
            this.isArrival=isArrival;
            this.context=context;
        }
        @Override
        public void onTimeSet(TimePicker timePicker, int hour, int min) {
            String time = hour+":"+min;
            if(isArrival){
                arrivaltime.setText(time);
                if(validateTime(time)){
                    intervention.setArrival_time(time);
                }else {
                    Toast.makeText(context,"Arrival Time needs to be before Departure time", Toast.LENGTH_SHORT).show();
                    arrivaltime.setText("");
                }

             }else{
                    departuretime.setText(time);
                    if(validateTime(time)){
                        intervention.setDeparture_time(time);
                    }else {
                        Toast.makeText(context,"Departure Time needs to be after Arrival time", Toast.LENGTH_SHORT).show();
                        departuretime.setText("");
                    }
            }

        }

        /**
         * Validate the time for Arrival_time and Departure_time. Departure time needs to be after arrival_time
         */
        private boolean validateTime(String time){
            DateFormat sdf = new SimpleDateFormat("hh:mm");
            Date arrival_time = new Date();
            Date departureTime = new Date();
            if(isArrival){
                try {
                    arrival_time = sdf.parse(time);
                } catch (ParseException e) {
                    e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                }
                //Time arrival_time = Time.valueOf(time);
                if(intervention.getDeparture_time()==null){
                    return true;
                }else{
                    //Time departureTime = Time.valueOf(intervention.getDeparture_time());
                    try{
                       departureTime = sdf.parse(intervention.getDeparture_time());
                    }catch (ParseException e){
                        e.printStackTrace();
                    }
                    int comparison = arrival_time.compareTo(departureTime);
                    if(comparison>0 || comparison==0){
                       return false;
                    }else {
                        return true;
                    }
                }
            }else{
                try {
                    departureTime = sdf.parse(time);
                } catch (ParseException e) {
                    e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                }

                if(intervention.getArrival_time()==null){
                    return true;
                }else{
                    //Time arrival_time = Time.valueOf(intervention.getArrival_time());
                    try {
                        arrival_time = sdf.parse(intervention.getArrival_time());
                    } catch (ParseException e) {
                        e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                    }

                    int comparison = departureTime.compareTo(arrival_time);

                    if(comparison<0 ||comparison==0 ){
                        return false;
                    }else{
                        return true;
                    }
                }
            }
        }

    }
}
