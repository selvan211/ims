package com.example.ERP_DEMO;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.TextView;

/**
 * Created with IntelliJ IDEA.
 * User: arthur
 * Date: 3/1/13
 * Time: 3:26 PM
 * To change this template use File | Settings | File Templates.
 */
public class InterventionDisplayActivity extends Activity {

    private String userID="";
    private String username="";
    private TextView doneBy;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.display_intervention);

        Intent i = getIntent();
        String caseID = i.getStringExtra("case_id");
        int itrvID = i.getExtras().getInt("itvr_id");

        doneBy = (TextView) findViewById(R.id.doneBy);

        InterventionDisplayService interventionDisplayService = new InterventionDisplayService(caseID,itrvID);
        interventionDisplayService.setActivityContext(InterventionDisplayActivity.this);
        interventionDisplayService.execute();

        TextView itvr_id = (TextView) findViewById(R.id.case_id);
        itvr_id.setText("Intervention Report:"+ String.valueOf(itrvID));


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflator = getMenuInflater();
        inflator.inflate(R.menu.display_intervention_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem){
        //Handle item selection
        switch (menuItem.getItemId()){
            case R.id.backToMainMenu:
                //do something
                setUsername_id(doneBy.getText().toString());
                Intent mainMenu = new Intent(getApplicationContext(), CasesActivity.class);
                mainMenu.putExtra("username",getUsername());
                mainMenu.putExtra("userId",getUserID());
                mainMenu.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                mainMenu.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(mainMenu);
                finish();

                return true;
            default:
                return true;
        }
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public void setUsername_id(String result){

       String[] parts = result.split(",");
       setUsername(parts[1]);
       setUserID(parts[0]);
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getUserID() {
        return userID;
    }

    public String getUsername() {
        return username;
    }
}