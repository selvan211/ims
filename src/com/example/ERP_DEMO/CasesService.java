package com.example.ERP_DEMO;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.CookieStore;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.protocol.ClientContext;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: arthur
 * Date: 12/18/12
 * Time: 2:41 PM
 * To change this template use File | Settings | File Templates.
 */
public class CasesService extends AsyncTask<Void, Void, ArrayList<Case>> {

//public class CasesService{

    private static ArrayList<Case> cases = new ArrayList<Case>();
    private String userID;
    private String session_id;
    private static String result="";
    private int casesAmount = 0;
    private Activity activity;
    //The link to the showCases.php
    public static final String showCases_URL = "http://mobile.dcl.mu/webserver/showCasesTest.php";
    private ProgressDialog progressDialog;

    public CasesService(String userId, String session_id){
        this.userID=userId;
        this.session_id=session_id;
    }

    @Override
    protected ArrayList<Case> doInBackground(Void... voids) {

        HttpClient httpClient = new DefaultHttpClient();
        HttpPost httpPost = new HttpPost(showCases_URL);
        HttpParams httpParams = new BasicHttpParams();
        HttpConnectionParams.setConnectionTimeout(httpParams, 10000);
        HttpConnectionParams.setSoTimeout(httpParams,10000);

        try {

            List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
            nameValuePairs.add(new BasicNameValuePair("userID", userID));
            nameValuePairs.add(new BasicNameValuePair("session_id", session_id));
            httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs));

            CookieStore cookieStore = new BasicCookieStore();
            HttpContext httpContext = new BasicHttpContext();
            httpContext.setAttribute(ClientContext.COOKIE_STORE,cookieStore);

            HttpResponse httpResponse = httpClient.execute(httpPost,httpContext);

            HttpEntity entity = httpResponse.getEntity();
            //convertToJson(result);

            if (entity != null) {
                result = EntityUtils.toString(httpResponse.getEntity());
                JSONObject jsonObject = new JSONObject(result);

                JSONObject object;

                JSONArray casesArray = jsonObject.getJSONArray("Cases");

                if(casesArray.length() ==0){
                        casesAmount= 0;
                }else{
                    //The amount of cases for the user
                    casesAmount= casesArray.length();

                    /**
                     *  Clearing the ArrayList
                     *  The service can be called several times,
                     *  so clearing the CasesArray will get rid of duplicates
                     */
                    cases.clear();

                    for(int i = 0;i<casesArray.length();i++){

                        object = casesArray.getJSONObject(i);

                        //Verifying the success
                        if(object.getInt("success")==0){
                            casesAmount = 0;
                            break;
                        }else{
                            //Creating the Case object with its attributes
                            Case aCase = new Case();

                            //TODO
                            String test = object.getString("session_id");

                            aCase.setCase_id(object.getString("cases_id"));
                            aCase.setAccount_id(object.getString("cases_account_id"));
                            aCase.setReferences(object.getString(("cases_references")));
                            aCase.setCase_name(object.getString(("cases_name")));
                            aCase.setCase_details(object.getString("description"));
                            aCase.setDate_created(object.getString(("date_created")));
                            aCase.setDate_modified(object.getString(("date_modified")));
                            aCase.setTeam_name(object.getString(("team_short_name")));
                            aCase.setAccount_name(object.getString(("account_name")));
                            aCase.setActivity_type(object.getString(("activity_type")));
                            aCase.setPriority(object.getString(("priority")));
                            aCase.setC_user(object.getString(("c_user")));
                            aCase.setM_user(object.getString(("m_user")));

                            //Adding each Case Object to the ArrayList
                            cases.add(aCase);
                        }
                    }
                }
            }else{
                Log.e("HttpResponse incorrect","Entity null");
            }
        }catch (IOException e){
            Log.e("Cases_Service", e.toString());
        }catch (JSONException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        return cases;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();    //To change body of overridden methods use File | Settings | File Templates.
        progressDialog = ProgressDialog.show(activity,"","Loading Cases List");
    }

    //Used to update the CasesActivity fields
    protected void onPostExecute(ArrayList<Case> cases){
        ListView listView = (ListView) activity.findViewById(R.id.listView);
        TextView textView = (TextView) activity.findViewById(R.id.casesAmount);

        //Setting the Adapter and the list with the correct information
        ArrayAdapter<Case> adapter = new ArrayAdapter<Case>(activity, android.R.layout.simple_list_item_1, cases);
        listView.setAdapter(adapter);
        textView.setText("Number of Cases: "+casesAmount+"");
        progressDialog.dismiss();
    }

    public ArrayList<Case> getCasesList(){
        return cases;
    }

    public void setActivityContext(Activity newContext){
         activity = newContext;
    }
}
