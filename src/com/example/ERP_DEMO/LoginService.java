package com.example.ERP_DEMO;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.util.Log;
import android.view.Gravity;
import android.widget.Toast;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.CookieStore;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.protocol.ClientContext;
import org.apache.http.cookie.Cookie;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: arthur
 * Date: 2/12/13
 * Time: 10:09 PM
 * To change this template use File | Settings | File Templates.
 */
class LoginService extends AsyncTask<String, Void, String> {

    private static final String loginURI="http://mobile.dcl.mu/webserver/login.php";
    private String username;
    private String password;
    private Activity context;
    private int loginSuccess;
    private String userId;
    private String session_id = "";
    private ProgressDialog progressDialog;

    public LoginService(String username, String password){
        this.username=username;
        this.password=password;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();    //To change body of overridden methods use File | Settings | File Templates.
        progressDialog = ProgressDialog.show(context, "Verifying login credentials", "Please wait...");
    }


    @Override
    protected String doInBackground(String... params) {

        return login(username, password);
    }


    //After the background work
    protected void onPostExecute(String result){
        //LoginSuccess == 1 when the login is successful
        if (loginSuccess==1){
           Intent casesActivity = new Intent(context, CasesActivity.class);
           casesActivity.putExtra("username", result);
           casesActivity.putExtra("userId", userId);
           casesActivity.putExtra("session_id",session_id );
           casesActivity.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
           casesActivity.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
           context.startActivity(casesActivity);
           context.finish();
        }else{
          Toast toast = Toast.makeText(context, "Incorrect Details", Toast.LENGTH_LONG);
          toast.setGravity(Gravity.CENTER_HORIZONTAL,0,5);
          toast.show();
        }

        progressDialog.dismiss();
    }


    //Does the HTTPRequest to the web-service by sending the username and password
    private String login(String username, String password){

        String result = "";

        HttpClient httpClient = new DefaultHttpClient();
        HttpPost httpPost = new HttpPost(loginURI);
        HttpParams httpParams = new BasicHttpParams();
        HttpConnectionParams.setConnectionTimeout(httpParams, 10000);
        HttpConnectionParams.setSoTimeout(httpParams,10000);

        try {

            List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
            nameValuePairs.add(new BasicNameValuePair("username", username));
            nameValuePairs.add(new BasicNameValuePair("password", password));
            httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs));

            /**
             * NOT SURE ABOUT THIS. NEED TO BE CERTAIN
             */

            CookieStore cookieStore = new BasicCookieStore();
            HttpContext httpContext = new BasicHttpContext();
            httpContext.setAttribute(ClientContext.COOKIE_STORE,cookieStore);

            HttpResponse httpResponse = httpClient.execute(httpPost, httpContext);

            HttpEntity entity = httpResponse.getEntity();

            if (entity != null) {
                result = EntityUtils.toString(httpResponse.getEntity());

                JSONObject jsonObject = new JSONObject(result);

                JSONArray jsonArray = jsonObject.getJSONArray("Username");
                JSONObject j_userId = jsonArray.getJSONObject(0);

                userId = j_userId.getString("id");
                //result = j_userId.getString("id") + ":-"+j_userId.getString("first_name");
                result = j_userId.getString("first_name");
                loginSuccess = j_userId.getInt("success");

                List<Cookie> cookieList = cookieStore.getCookies();
                for(int i=0;i<cookieList.size();i++){
                    session_id = cookieList.get(i).getValue();
                }

             }else{
               Log.e("HttpResponse incorrect","Entity null");
            }



        }catch (IOException e){
            Log.e("Login_Issue", e.toString());
        }catch (JSONException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        return result;
    }

    public void setActivity(Activity newContext){
        context = newContext;
    }

}
