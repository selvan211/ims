package com.example.ERP_DEMO;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.*;
import android.widget.*;

import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 * User: arthur
 * Date: 2/19/13
 * Time: 8:45 PM
 * To change this template use File | Settings | File Templates.
 */
public class CaseInformationActivity extends Activity {

    private Case aCase;
    private String userID;
    private Dialog dialog;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.case_info);

        Intent i = getIntent();
        aCase = (Case) i.getSerializableExtra("Case");
        userID = i.getStringExtra("userID");

        dialog = new Dialog(this);
        dialog.setTitle("Select an Intervention report");

        //Creating the textViews for the view
        TextView caseID = (TextView) findViewById(R.id.caseID);
        TextView accountId = (TextView) findViewById(R.id.accountID);
        TextView case_name = (TextView) findViewById(R.id.caseName);
        TextView date_modified = (TextView) findViewById(R.id.dateModified);
        TextView accountName = (TextView) findViewById(R.id.accountName);
        TextView activityType = (TextView) findViewById(R.id.activityType);
        TextView priority = (TextView) findViewById(R.id.priority);
        TextView teamName = (TextView) findViewById(R.id.teamName);
        TextView case_details = (TextView) findViewById(R.id.caseDetails);

        //Setting each textView with the correct information from object Case
        caseID.setText(aCase.getReferences());
        accountId.setText(aCase.getAccount_id());
        case_name.setText(aCase.getCase_name());
        date_modified.setText(aCase.getDate_modified());
        accountName.setText(aCase.getAccount_name());
        activityType.setText(aCase.getActivity_type());
        priority.setText(aCase.getPriority());
        teamName.setText(aCase.getTeam_name());
        case_details.setText(aCase.getCase_details());

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflator = getMenuInflater();
        inflator.inflate(R.menu.case_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem){
        //Handle item selection
        switch (menuItem.getItemId()){
            case R.id.createIntervention:
                    //do something
                    Intent createIntervention = new Intent(this, InterventionReport.class);
                    createIntervention.putExtra("Case", aCase);
                    createIntervention.putExtra("userID", userID);
                    createIntervention.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(createIntervention);
                    return true;
            case R.id.viewLastITVs:
                    //do something else
                    LayoutInflater inflaterPop = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

                    View v = inflaterPop.inflate(R.layout.itvrs_list, null, true);

                    dialog.setContentView(v);
                    dialog.setCancelable(true);
                    dialog.show();

                    InterventionReportListService interventionReportList = new InterventionReportListService(aCase.getCase_id());
                    interventionReportList.setActivity(CaseInformationActivity.this);
                    interventionReportList.setDialog(dialog);
                    interventionReportList.execute();


                    //AlertDialog.Builder builder = new AlertDialog.Builder(this);

                    final ArrayList<Intervention> interventions = interventionReportList.getInterventionArrayList();

                    ListView list1 = (ListView) dialog.findViewById(R.id.itvr_listView);
                    list1.setClickable(true);
                    list1.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                            dialog.dismiss();
                            Intervention intervention = interventions.get(i);

                            Intent interventionDisplay = new Intent(getApplicationContext(), InterventionDisplayActivity.class);
                            interventionDisplay.putExtra("itvr_id", Integer.parseInt(intervention.getItvr_id()));
                            interventionDisplay.putExtra("case_id", intervention.getCase_id());
                            //interventionDisplay.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            interventionDisplay.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(interventionDisplay);
                            //finish();
                        }
                    });

                return true;
            default:
                 return true;
        }
    }




}
