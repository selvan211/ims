package com.example.ERP_DEMO;

import com.google.gson.annotations.SerializedName;

/**
 * Created with IntelliJ IDEA.
 * User: arthur
 * Date: 2/24/13
 * Time: 11:15 PM
 * To change this template use File | Settings | File Templates.
 */
public class Intervention{

    @SerializedName("itvr_id")
    private String itvr_id;
    public String getItvr_id(){
      return itvr_id;
    }

    @SerializedName("case_id")
    private String case_id;
    public String getCase_id() {
        return case_id;
    }

    @SerializedName("user_id")
    private String user_id;
    public String getUser_id() {
        return user_id;
    }

    @SerializedName("account_id")
    private String account_id;
    public String getAccount_id() {
        return account_id;
    }

    @SerializedName("description")
    private String description;
    public String getDescription() {
        return description;
    }

    @SerializedName("solution")
    private String solution;
    public String getSolution() {
        return solution;
    }

    @SerializedName("status")
    private String status;
    public String getStatus() {
        return status;
    }

    @SerializedName("user_remarks")
    private String user_remarks;
    public String getUser_remarks() {
        return user_remarks;
    }


    @SerializedName("customer_remarks")
    private String customer_remarks;
    public String getCustomer_remarks() {
        return customer_remarks;
    }

    @SerializedName("case_complete")
    private boolean case_complete;
    public boolean isCase_complete() {
        return case_complete;
    }

    @SerializedName("charge_transport")
    private boolean charge_transport;
    public boolean isCharge_transport() {
        return charge_transport;
    }

    @SerializedName("service_billable")
    private boolean service_billable;
    public boolean isService_billable() {
        return service_billable;
    }

    @SerializedName("date_created")
    private String date_created;
    public String getDate_created() {
        return date_created;
    }

    @SerializedName("arrival_time")
    private String arrival_time;
    public String getArrival_time() {
        return arrival_time;
    }

    @SerializedName("departure_time")
    private String departure_time;
    public String getDeparture_time() {
        return departure_time;
    }

    @SerializedName("user_sign")
    private String user_sign;
    public String getUser_sign() {
        return user_sign;
    }

    @SerializedName("cust_sign")
    private String cust_sign;
    public String getCust_sign() {
        return cust_sign;
    }
    @SerializedName("parts_delivered")
    private String parts_delivered;
    public String getParts_delivered() {
        return parts_delivered;
    }

    public void setParts_delivered(String parts_delivered) {
        this.parts_delivered = parts_delivered;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public void setCase_id(String case_id) {
        this.case_id = case_id;
    }


    public void setAccount_id(String account_id) {
        this.account_id = account_id;
    }

    public void setItvr_id(String itvr_id){
        this.itvr_id = itvr_id;
    }



    public void setDescription(String description) {
        this.description = description;
    }



    public void setSolution(String solution) {
        this.solution = solution;
    }



    public void setStatus(String status) {
        this.status = status;
    }


    public void setUser_remarks(String user_remarks) {
        this.user_remarks = user_remarks;
    }



    public void setCustomer_remarks(String customer_remarks) {
        this.customer_remarks = customer_remarks;
    }



    public void setCase_complete(boolean case_complete) {
        this.case_complete = case_complete;
    }



    public void setCharge_transport(boolean charge_transport) {
        this.charge_transport = charge_transport;
    }

    public void setService_billable(boolean service_billable) {
        this.service_billable = service_billable;
    }


    public void setDate_created(String date_created) {
        this.date_created = date_created;
    }

    public void setArrival_time(String arrival_time) {
        this.arrival_time = arrival_time;
    }


    public void setDeparture_time(String departure_time) {
        this.departure_time = departure_time;
    }

    public void setUser_sign(String user_sign) {
        this.user_sign = user_sign;
    }

    public void setCust_sign(String cust_sign) {
        this.cust_sign = cust_sign;
    }

    @Override
    public String toString() {
        return  itvr_id + " :\t " +date_created +" \n " + "\t" + description;
    }
}
