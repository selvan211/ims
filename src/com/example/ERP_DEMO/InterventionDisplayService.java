package com.example.ERP_DEMO;

import android.app.Activity;
import android.app.ProgressDialog;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.CookieStore;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.protocol.ClientContext;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: arthur
 * Date: 3/1/13
 * Time: 3:54 PM
 * To change this template use File | Settings | File Templates.
 */
public class InterventionDisplayService extends AsyncTask<Void,Integer,String> {

    private String case_id;
    private int itvr_id;
    private static final String displayIntervention = "http://mobile.dcl.mu/webserver/displayIntervention.php";
    private int success;
    private Activity activity;
    private Intervention intervention = new Intervention();
    private String account_name ="";
    private String u_username="";
    private String reference="";
    private ProgressBar progressBar;
    private String result;
    private ProgressDialog progressDialog;

    public InterventionDisplayService(String case_id, int itvr_id){
        this.case_id=case_id;
        this.itvr_id=itvr_id;
    }

    @Override
    protected String doInBackground(Void... voids) {

        HttpClient httpClient = new DefaultHttpClient();
        HttpPost httpPost = new HttpPost(displayIntervention);
        HttpParams httpParams = new BasicHttpParams();
        HttpConnectionParams.setConnectionTimeout(httpParams, 10000);
        HttpConnectionParams.setSoTimeout(httpParams,10000);

        try {

            List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
            nameValuePairs.add(new BasicNameValuePair("itvr_id",String.valueOf(itvr_id)));
            nameValuePairs.add(new BasicNameValuePair("case_id",case_id));
            httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs));

            CookieStore cookieStore = new BasicCookieStore();
            HttpContext httpContext = new BasicHttpContext();
            httpContext.setAttribute(ClientContext.COOKIE_STORE,cookieStore);

            HttpResponse httpResponse = httpClient.execute(httpPost,httpContext);

            HttpEntity entity = httpResponse.getEntity();

            if (entity != null) {
                String result = EntityUtils.toString(httpResponse.getEntity());
                JSONObject jsonObject = new JSONObject(result);
                publishProgress(10);
                success = jsonObject.getInt("success");
                //This means that the insertion has been successful
                if(success==1){
                    intervention.setUser_id(jsonObject.getString("user_id"));
                    intervention.setCase_id(jsonObject.getString("case_id"));
                    intervention.setAccount_id(jsonObject.getString("account_id"));
                    intervention.setDescription(jsonObject.getString("description"));
                    intervention.setSolution(jsonObject.getString("solution"));
                    intervention.setParts_delivered(jsonObject.getString("parts_delivered"));
                    intervention.setStatus(jsonObject.getString("status"));
                    intervention.setUser_remarks(jsonObject.getString("user_remarks"));
                    intervention.setCustomer_remarks(jsonObject.getString("cust_remarks"));
                    intervention.setCase_complete(jsonObject.getBoolean("case_complete"));
                    intervention.setCharge_transport(jsonObject.getBoolean("charge_transport"));
                    intervention.setService_billable(jsonObject.getBoolean("service_billable"));
                    intervention.setDate_created(jsonObject.getString("date_created"));
                    intervention.setArrival_time(jsonObject.getString("arrival_time"));
                    intervention.setDeparture_time(jsonObject.getString("departure_time"));
                    intervention.setUser_sign(jsonObject.getString("user_sign"));
                    intervention.setCust_sign(jsonObject.getString("cust_sign"));
                    account_name = jsonObject.getString("account_name");
                    reference = jsonObject.getString("reference");
                    u_username = jsonObject.getString("u_username");
                }else{
                    Log.e("Fail to retrieve ID", String.valueOf(itvr_id));
                }

                publishProgress(25);

            }
        }catch (IOException e){
            Log.e("Cases_Service", e.toString());
        } catch (JSONException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }

        return result;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();    //To change body of overridden methods use File | Settings | File Templates.
        progressDialog = ProgressDialog.show(activity,"Loading","Loading Intervention Report");

    }

    @Override
    protected void onProgressUpdate(Integer... progress) {
        super.onProgressUpdate(progress);    //To change body of overridden methods use File | Settings | File Templates.

    }

    @Override
    protected void onPostExecute(String result) {
        super.onPostExecute(result);    //To change body of overridden methods use File | Settings | File Templates.


        TextView date = (TextView) activity.findViewById(R.id.date);
        TextView case_id = (TextView) activity.findViewById(R.id.caseID);
        TextView accountName = (TextView) activity.findViewById(R.id.accountName);
        TextView description = (TextView) activity.findViewById(R.id.description);
        TextView solution = (TextView) activity.findViewById(R.id.editSolution);
        TextView showStatusView = (TextView) activity.findViewById(R.id.showStatusView);
        TextView arrivalTime = (TextView) activity.findViewById(R.id.arrivalTime);
        TextView departureTime = (TextView) activity.findViewById(R.id.departuretime);
        TextView user_remarks = (TextView) activity.findViewById(R.id.userRemark);
        TextView cust_remarks = (TextView) activity.findViewById(R.id.customerRemark);
        TextView parts_delivered = (TextView) activity.findViewById(R.id.partsDelivered);
        TextView doneBY = (TextView) activity.findViewById(R.id.doneBy);

        CheckBox caseComplete = (CheckBox) activity.findViewById(R.id.checkBoxCaseComplete);
        CheckBox chargeTransport = (CheckBox) activity.findViewById(R.id.checkBoxChargeTransport);
        CheckBox serviceBillable = (CheckBox) activity.findViewById(R.id.checkBoxServiceBillable);

        ImageView cust_sign = (ImageView) activity.findViewById(R.id.imageViewCustSign);
        ImageView user_sign = (ImageView) activity.findViewById(R.id.imageViewUserSign);

        //Populating the view on InterventionDisplayActivity
        case_id.setText(reference);
        accountName.setText(account_name);
        description.setText(intervention.getDescription());
        solution.setText(intervention.getSolution());
        showStatusView.setText(intervention.getStatus());
        user_remarks.setText(intervention.getUser_remarks());
        cust_remarks.setText(intervention.getCustomer_remarks());
        parts_delivered.setText(intervention.getParts_delivered());
        doneBY.setText(intervention.getUser_id() + "," + u_username);

        String date_format = intervention.getDate_created();
        String[] date_created = date_format.split(" ");

        date.setText("Date created: "+ date_created[0]);
        arrivalTime.setText(intervention.getArrival_time());
        departureTime.setText(intervention.getDeparture_time());


        if(intervention.isCase_complete()){
            caseComplete.setChecked(true);
        }
        caseComplete.setEnabled(false);
        if(intervention.isCharge_transport()){
            chargeTransport.setChecked(true);
        }
        chargeTransport.setEnabled(false);
        if(intervention.isService_billable()){
            serviceBillable.setChecked(true);
        }
        serviceBillable.setEnabled(false);

        byte[] decodedUserString = Base64.decode(intervention.getUser_sign(), Base64.NO_WRAP);
        Bitmap decoded_user_sign = BitmapFactory.decodeByteArray(decodedUserString, 0, decodedUserString.length);
        user_sign.setImageBitmap(decoded_user_sign);

        byte[] decodedCustString = Base64.decode(intervention.getCust_sign(), Base64.NO_WRAP);
        Bitmap decoded_cust_sign = BitmapFactory.decodeByteArray(decodedCustString, 0, decodedCustString.length);
        cust_sign.setImageBitmap(decoded_cust_sign);

        date.setVisibility(View.VISIBLE);
        case_id.setVisibility(View.VISIBLE);
        accountName.setVisibility(View.VISIBLE);
        description.setVisibility(View.VISIBLE);
        solution.setVisibility(View.VISIBLE);
        showStatusView.setVisibility(View.VISIBLE);
        arrivalTime.setVisibility(View.VISIBLE);
        departureTime.setVisibility(View.VISIBLE);
        user_remarks.setVisibility(View.VISIBLE);
        cust_remarks.setVisibility(View.VISIBLE);
        parts_delivered.setVisibility(View.VISIBLE);

        caseComplete.setVisibility(View.VISIBLE);
        chargeTransport.setVisibility(View.VISIBLE);
        serviceBillable.setVisibility(View.VISIBLE);

        progressDialog.dismiss();
    }


    public void setActivityContext(Activity newContext){
        activity = newContext;
    }

}
